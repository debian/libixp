#!/bin/sh -e

TARBALLDIR=${1:-.}
UPSTREAM_REPO=https://code.google.com/p/libixp/
PACKAGE=libixp
SRC_VERSION=$(dpkg-parsechangelog | sed -ne 's/^Version: \(\([0-9]\+\):\)\?\(.*\)-.*/\3/p')
SRC_REVISION=${SRC_VERSION##*hg}
TARBALL=$(readlink -f "$TARBALLDIR/${PACKAGE}_${SRC_VERSION}.orig.tar.bz2")
REPODIR="debian/orig-source/${PACKAGE}-${SRC_VERSION}.orig"
if [ -e "$REPODIR" ]
then
    echo "$REPODIR directory found, not removing. Aborted."
    exit 1
fi
mkdir -p debian/orig-source
echo "Cloning ${UPSTREAM_REPO}"
hg clone "${UPSTREAM_REPO}" "$REPODIR" || exit 1
echo "Creating ${TARBALL} for revision $SRC_REVISION"
cd "$REPODIR"
hg archive -r "$SRC_REVISION" -X".hg*" "${TARBALL}" || exit 1
cd "$OLDPWD"
rm -rf debian/orig-source

